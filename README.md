***Summary***

This is a bash script for configuring a linux system with a running PulseAudio sound server.\
The configuration allows recording of software audio output without having to record all system audio.

***Why***

Recording some kind of audio coming from software on your computer is a common task for even novice computer users.\
PulseAudio is the sound server on most linux distros and it works well, but it is difficult for new users to understand.\
Sometimes the simple option (record all system audio) is not convenient (for example, when multiple audio streams are going at once).\
I already had a PulseAudio script written with much of the functionality needed so this only required minor changes to make.\
I am happy to provide this work which will hopefully help you and others.

***How it works***

The script is thoroughly commented but I will provide a brief explanation here.\
PulseAudio can be controlled/configured on the fly with the pacmd and pactl commands.\
These commands can be used to create virtual devices and make connections between devices.

In this script, these commands are used as follows:\
-The script creates a virtual sink.\
-The script can set the monitor of the sink as the system default audio source.\
-The script can switch the audio output from your desired program to the new sink.\
-The script can set the volume level of the new source to any value.\

The script frequently prompts the user for input as some steps may already have been taken.\
If you follow all the steps, when you record audio the output of your selected program will be recorded.\
You will not hear the audio as it's being played and will be able to play and hear all other audio at the same time.\
At the end of the script, all of the changed settings are reverted to their previous state.


