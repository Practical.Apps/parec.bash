#!/bin/bash
echo -n "Proceed with configuring PulseAudio for recording system audio? (y/n): "
read ans_on
if [ "$ans_on" = y ]
then
  DEFAULT_SOURCE=`pacmd dump | mawk '/set-default-source/ {print $2}'`
  DEFAULT_SINK=`pacmd dump | mawk '/set-default-sink/ {print $2}'`
  REC_NAME="Record"
  mod_num=77777

  # Create a virtual sink and record the ID for use later
  echo "Creating recording module..."
  mod_num=$(pactl load-module module-null-sink sink_name=$REC_NAME sink_properties=device.description="$REC_NAME")
  echo "Module created [ID: $mod_num]"

  # Question user if they want to switch default audio source to the monitor of the new sink
  echo -n "Set $REC_NAME module as the system default audio source? (y/n): "
  read ans_source
  if [ "$ans_source" = y ]
  then
    # Switch Default source to new virtual source
    pactl set-default-source $REC_NAME.monitor
    echo "Default audio source switched."
  else
    # User would have to change default output manually
    echo "Staying on default source channel: $DEFAULT_SOURCE"
  fi

  # Prompt the user to queue up audio on their system so it becomes available as a list entry
  echo "***Before proceeding with the next to the next step***"
  echo "Begin playing the audio you would like to record (you can pause it after starting)."
  echo "That way your audio program will appear on the PulseAudio client list"

  # Assign client audio output to the newly made virtual sink
  echo -n "Change your program output to the $REC_NAME sink? (y/n): "
  read ans_opt
  if [ "$ans_opt" = y ]
  then
    # Display the client list for the user to select from
    echo "The following is the list of programs interfacing with the system audio"
    echo "Enter the number corresponding to the program whose audio you want to record"
    pactl list short clients | mawk '{print "[" $1 "] " $3}'
    # Create an array of the ID numbers to check against
    CLNT_ID_LIST=($(pactl list short clients | mawk '{ print $1 }'))
    echo -n "Enter client ID number: "
    read ans_num
    # Regular expression to check if entry was a number
    if ! [[ "$ans_num" =~ ^[0-9]+$ ]]
      then
        echo "Error: Not a number, nothing will be changed"
      else
        ans_val=n
        # Cycle through array to check that given number is valid
        for i in "${CLNT_ID_LIST[@]}"
        do
          if  [ $i = "$ans_num" ]
            then
              ans_val=y
              break
          fi
        done
        # If given number is a valid ID then proceed with change
        if  [ "$ans_val" = y ]
          then
            # Find entry in sink-inputs list corresponding to the given client ID
            CLNT_STRM_ID=$(pactl list short sink-inputs | mawk -v CID=${ans_num} '$3 == CID {print $1}')
            # Find ID number of the new sink
            SINK_ID=$(pactl list short sinks | grep "$REC_NAME" | mawk '{print $1}')
            echo "Changing client [ID: $ans_num, stream ID: $CLNT_STRM_ID] output to $REC_NAME sink [ID: $SINK_ID]"
            # Change the output of the client to the new sink
            pactl move-sink-input $CLNT_STRM_ID $SINK_ID
            echo "Program output successfully changed"
          else
            echo "$ans_num is NOT a valid number"
        fi
    fi
  else
    # User would have to change program output manually
    echo "Program configuration skipped"
  fi

  # Offer option to adjust volume level for the new source
  echo -n "Set custom volume level for $REC_NAME source? (y/n): "
  read ans_cv
  if [ "$ans_cv" = y ]
  then
    echo -n "Enter volume as Int/150: "
    read ans_vol
    # Regular expression to check if entry was a number
    if ! [[ "$ans_vol" =~ ^[0-9]+$ ]]
      then 
        echo "error: Not a valid number, default will be 100%"
      else
        # Set volume to number entered (66000 is 100%, 99000/150% is max amplification)
        pactl set-source-volume  $REC_NAME.monitor $((ans_vol*660))
        echo "$REC_NAME.monitor volume adjusted from 100 to $ans_vol %"
    fi
  else
    # User would have to change sink volume manually
    echo "No custom value set, default will be 100%"
  fi

  # Prompt the user to undo changes
  # Kept within while loop in case of accidental user input
  ans_off=n
  while [ "$ans_off" != y ]
  do
    echo -n "Reverse changes? (y/n): "
    read ans_off
  done

  # Reset changes
  if [ "$ans_off" = y ]
  then
    # Reset default source
    pactl set-default-source $DEFAULT_SOURCE
    echo "Default audio input reset"
    # Unload new sink module
    pactl unload-module "$mod_num"
    echo "Module ${mod_num} unloaded"
  fi

else
  echo "PulseAudio configuration canceled"
fi
